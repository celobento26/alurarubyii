def da_boas_vindas
  puts "Bem vindo ao jogo da forca"
  puts "Qual é o seu nome?"
  nome = gets.strip
  puts "\n\n\n\n\n\n"
  puts "Começaremos o jogo para você, #{nome}"
  nome
end

def sorteia_palavra_secreta
  puts "Escolhendo uma palavra..."
  palavra_secreta = "programador"
  puts "Escolhida uma palavra com #{palavra_secreta.size} letras... boa sorte!"
  palavra_secreta
end

def nao_quer_jogar?
  puts "Deseja continuar jogando? (s/n)"
  resposta = gets.strip
  continua = resposta.upcase == "N"
  continuar
end

def verifica_letra_contida chute, palavra_secreta
  total_encontrado = 0
  for i in 0..(palavra_secreta.size - 1)
    letra = palavra_secreta[i]
    if letra == chute
        total_encontrado += 1
    end
  end

  if total_encontrado == 0
    puts "Letra não encontrada!"
    erros += 1
  else
      puts "Letra encontrada #{total_encontrado} vezes!"
  end

end

def joga(nome)
    palavra_secreta = sorteia_palavra_secreta
    erros = 0
    chutes = []
    pontos_ate_agora = 0

    while erros < 5
      chute = pede_um_chute chutes, erros
      if chutes.include? chute
            puts "Você já chutou #{chute}"
            next
        end
      chutes << chute
      if chute.size > 1
        acertou = chute == palavra_secreta
        if acertou == true
          puts "Parabéns! Acertou!"
          pontos_ate_agora += 100
          break
        else
          puts "Que pena... errou!"
          pontos_ate_agora -= 30
          erros += 1
        end

      else

        verifica_letra_contida chute, palavra_secreta

      end

    end

    puts "Você ganhou #{pontos_ate_agora} pontos."
end

def pede_um_chute(chutes, erros)
  puts "Chutes: #{chutes}"
  puts "Erros: #{erros}"
  puts "Chute uma letra."
  letra_chutada = gets.strip
letra_chutada
end

nome = da_boas_vindas

loop do
    joga nome
    break if nao_quer_jogar?
end
